package dataTypes;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.joda.time.DateTime;

import com.nykredit.kundeservice.data.DopeException;
import com.nykredit.kundeservice.data.KSDriftConnection;
import com.nykredit.kundeservice.data.sql.SqlQuery;
import com.nykredit.kundeservice.util.OperationAbortedException;

public class WebdeskCallSumary {

	private DateTime date;
			
	private WebdeskCampaign campaign;
	private WebdeskDepartment department;
	
	int callCount;
	int answeredCount;
	int answeredWithin30SecCount;
	int forwardToDirekteCount;
		
	public DateTime getDate(){
		return this.date;
	}
	
	public WebdeskCampaign getCampaign(){
		return this.campaign;
	}
	public WebdeskDepartment getDepartment(){
		return this.department;
	}
	
	public int getCallCount(){
		return this.callCount;
	}
	public int getAnsweredCount(){
		return this.answeredCount;
	}
	public int getAnsweredWithin30SecCount(){
		return this.answeredWithin30SecCount;
	}
	public int getForwardedToDirekteCount(){
		return this.forwardToDirekteCount;
	}

	public WebdeskCallSumary(DateTime date,
							 WebdeskCampaign campaign,
							 WebdeskDepartment department,
							 int callCount,
							 int answeredCount,
							 int answeredWithin30SecCount,
							 int forwardToDirekteCount){
		if(campaign == null) throw new IllegalArgumentException("Campaign cannot be null.");
		if(department == null) throw new IllegalArgumentException("Department cannot be null.");
		
		this.date = date;
		
		this.campaign = campaign;
		this.department = department;
		
		this.callCount = callCount;
		this.answeredCount = answeredCount;
		this.answeredWithin30SecCount = answeredWithin30SecCount;
		this.forwardToDirekteCount = forwardToDirekteCount;
	}

	public static ArrayList<WebdeskCallSumary> getWebdeskCalls(KSDriftConnection conn, 
															   DateTime dateFrom, 
															   DateTime dateTo) throws DopeException {
		ArrayList<WebdeskCallSumary> webdeskCalls = new ArrayList<WebdeskCallSumary>();
		
		ArrayList<WebdeskCampaign> campaigns = WebdeskCampaign.getAllCampaigns(conn);
		ArrayList<WebdeskDepartment> departments = WebdeskDepartment.getAllDepartments(conn);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		
		String sql = "";
		
		String sql = "SELECT " + 
					     "web.*, " +
					     "vt.TEAM " +
				     "FROM " + 
					     "KS_DRIFT.WEBDESK_CALL web " + 
				     "LEFT JOIN " + 
					     "KS_DRIFT.V_TEAM_DATO vt " + 
				     "ON " + 
					     "web.INITIALS = vt.INITIALER AND trunc(web.DATETIME) = vt.DATO " +
				     "WHERE " + 
					     "web.DATETIME >= '" + sdf.format(dateFrom) + "' AND web.DATETIME <= '" + sdf.format(dateTo) +"' AND DEPARTMENT_ID = 42";
		
		SqlQuery query = new SqlQuery(conn, sql);
		
		try {
			query.execute();
			
			while(query.next()){
				WebdeskCampaign campaign = WebdeskCampaign.FindCampaignById(campaigns, query.getInteger("CAMPAIGN_ID"));
				WebdeskDepartment department = WebdeskDepartment.FindDepartmentById(departments, query.getInteger("DEPARTMENT_ID"));
				
				webdeskCalls.add(new WebdeskCallSumary(query.getString("CALL_ID"), 
													   query.getDateTime("DATETIME"), 
													   query.getString("INITIALS"), 
													   query.getString("TEAM"), 
													   query.getInteger("DURATION"), 
													   query.getBoolean("PROACTIVE"), 
													   query.getInteger("RESPONSE_TIME"), 
													   query.getString("CUSTOMER_ID"), 
													   campaign, 
													   department));				
			}
		} catch (DopeException e) {
			throw new OperationAbortedException(e, "Error during load of webdesk calls.", "Der skete en fejl ved indlęsning af webdesk kald.");
		}
		
		return webdeskCalls;
	}
}