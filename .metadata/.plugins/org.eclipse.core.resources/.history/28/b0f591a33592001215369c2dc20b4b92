package dataTypes;

import java.util.ArrayList;

import org.joda.time.DateTime;

import com.nykredit.kundeservice.data.DopeException;
import com.nykredit.kundeservice.data.KSDriftConnection;
import com.nykredit.kundeservice.data.sql.OracleFormats;
import com.nykredit.kundeservice.data.sql.SqlQuery;
import com.nykredit.kundeservice.util.OperationAbortedException;

public class WebdeskCallSumary {

	public enum SummaryPeriod{
		DAY("D"), WEEK("W"), MONTH("M");
		
		private String format;
		
		private SummaryPeriod(String format){
			this.format = format;
		}
	}
	
	private DateTime date;
			
	int callCount;
	int answeredCount;
	int answeredWithin30SecCount;
	int forwardToDirekteCount;
		
	public DateTime getDate(){
		return this.date;
	}
		
	public int getCallCount(){
		return this.callCount;
	}
	public int getAnsweredCount(){
		return this.answeredCount;
	}
	public int getAnsweredByServiceCenterCount(){
		return this.answeredCount - this.forwardToDirekteCount;
	}
	public int getAnsweredWithin30SecCount(){
		return this.answeredWithin30SecCount;
	}
	public int getForwardedToDirekteCount(){
		return this.forwardToDirekteCount;
	}

	public WebdeskCallSumary(DateTime date,
							 int callCount,
							 int answeredCount,
							 int answeredWithin30SecCount,
							 int forwardToDirekteCount){
		this.date = date;
		
		this.callCount = callCount;
		this.answeredCount = answeredCount;
		this.answeredWithin30SecCount = answeredWithin30SecCount;
		this.forwardToDirekteCount = forwardToDirekteCount;
	}

	public static ArrayList<WebdeskCallSumary> getWebdeskCalls(KSDriftConnection conn, 
															   DateTime dateFrom, 
															   DateTime dateTo,
															   SummaryPeriod period) throws DopeException {
		ArrayList<WebdeskCallSumary> webdeskCalls = new ArrayList<WebdeskCallSumary>();
				
		String sql = "SELECT " +
						 "SUM_DATE, " +
					     "CALLS, " +
					     "ANSWERED, " +
					     "ANSWERED_WITHIN_30_SEC, " +
					     "FORWARDED_TO_DIREKTE " +
					 "FROM " +
					     "KS_DRIFT_V_WEBDESK_SERVICCENTER " +
					 "WHERE " +
					     "DATE BETWEEN " + OracleFormats.convertDateTime(dateFrom) + " AND " + OracleFormats.convertDateTime(dateTo) + " " +
					 "GROUP BY " +
					 	 "TO_CHAR(DATE, 'YYYY'), " +
					 	"TO_CHAR(DATE, '" + period.format + "'), " +
					 	"MIN(DATE)";
		
		SqlQuery query = new SqlQuery(conn, sql);
		
		try {
			query.execute();
			
			while(query.next()){				
				webdeskCalls.add(new WebdeskCallSumary(query.getDateTime("START_OF_PERIOD"), 
													   query.getInteger("CALLS"),
													   query.getInteger("ANSWERED"),
													   query.getInteger("ANSWERED_WITHIN_30_SEC"),
													   query.getInteger("FORWARDED_TO_DIREKTE")));				
			}
		} catch (DopeException e) {
			throw new OperationAbortedException(e, "Error during load of webdesk calls.", "Der skete en fejl ved indlęsning af webdesk kald.");
		}
		
		return webdeskCalls;
	}
}