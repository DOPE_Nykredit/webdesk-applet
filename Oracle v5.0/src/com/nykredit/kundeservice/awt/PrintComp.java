package com.nykredit.kundeservice.awt;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.awt.print.PageFormat;
import java.util.ArrayList;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.standard.Chromaticity;
import javax.print.attribute.standard.OrientationRequested;
import javax.swing.JComponent;

/*
class PrintComp - This class is the subclass of the application. This class implements the Printable interface and prints a component.
Methods:
printComponent() - Invokes the print() method of PrintComp class and prints a component.
pageSetupAndPrint() - Allows the end user to specify the page setup and print the page.
print() - Prints the page.
print()-Prints a graphic at the specified page index in the specified page format.
*/

public class PrintComp implements Printable {
	/* Declares object of the JComponent class.*/
	private ArrayList<JComponent> components = new ArrayList<JComponent>();
	
	/* Declares object of the PrinterJob class.*/
	PrinterJob printJob = PrinterJob.getPrinterJob();
	
	/* Declares object of the PageFormat class*/
	PageFormat pageFormat;
	/* printComponent - This method is called when the end user clicks the Print Default menu item in the user interface of PrintFile.
	Parameters: 
	c - An object of the JComponent class.
	Return Value: NA */
	
	public static void printComponent(JComponent c, String printJobName) {
		ArrayList<JComponent> components = new ArrayList<JComponent>();
		
		components.add(c);
		
		new PrintComp(components, printJobName).print();
	}
	public static void printComponents(ArrayList<JComponent> c, String printJobName){
		new PrintComp(c, printJobName).print();
	}

	/* Implements the constructor of the PrintComp class. */
	public PrintComp(ArrayList<JComponent> component, String printJobName) {
		this.components= component;
		
		HashPrintRequestAttributeSet printAttributes = new HashPrintRequestAttributeSet();
		printAttributes.add(Chromaticity.COLOR);
		printAttributes.add(OrientationRequested.LANDSCAPE);
		
		this.printJob.setJobName(printJobName);
		
		this.pageFormat = this.printJob.getPageFormat(printAttributes);
	}

	/* pageSetupAndPrint - This method is called when the end user clicks the Page Setup and Print button.
	Parameters: NA
	Return Value: NA*/
	public void pageSetupAndPrint() throws PrinterException {
		/* Initializes the object of the PageFormat class. */
		pageFormat = printJob.pageDialog(pageFormat);
		
		/* Sets the object of the PrinterJob class to printable. */
		printJob.setPrintable(this, pageFormat);
		
		if (printJob.printDialog()) {
			try {
				/* Prints the document. */
				printJob.print();
			} catch(PrinterException pe) {
				System.out.println("Error in printing !!! " + pe);
			}
		}
	}
	
	/* print - This method is called when the end user clicks the Print button.
	Parameters: NA
	Return Value: NA */
 
	public void print() {
		/* Sets the object of the PrinterJob class to printable. */
		printJob.setPrintable(this, pageFormat);
		
		/* Checks the return value of PrintDialog. */
		if (printJob.printDialog()) {
			try {
				/* Prints the document. */
				printJob.print();
			} catch(PrinterException pe) {
				System.out.println("Error in printing !!! " + pe);
			}
		}
	}
	
	/* print() - This method defines a Printable interface.
	Parameters: 
	g - Represents the object of the Graphics class.
	pf - Represents the object of the PageFormat class.
	index - Represents an index of the page.
	Return Value: int PAGE_EXIST */ 
	public int print(Graphics g, PageFormat pf, int pageIndex) throws PrinterException {
		if(pageIndex >= this.components.size())
			return NO_SUCH_PAGE;
			
		JComponent componentToPrint = this.components.get(pageIndex);
		
		// Creates an object of the Graphics2D class and convert simple graphics to 2D graphics. 
		Graphics2D g2 = (Graphics2D)g;

		/* Gets the width of document in pixels. */
		double componentWidth = componentToPrint.getWidth(); 

		/* Gets the height of document in pixels. */
		double componentHeight = componentToPrint.getHeight();

		/* Gets the height of printer page. */
		double pageHeight = pf.getImageableHeight();

		/* Get the width of printer page*/
		double pageWidth = pf.getImageableWidth(); 

		// Sets the value of scale for the document to be printed.
		double widthScale = pageWidth/componentWidth;
		double heightScale = pageHeight/componentHeight;
		
		double scale = (widthScale > heightScale ? heightScale : widthScale);
		
//		int pages = (int)Math.ceil(scale * componentHeight / pageHeight);

		/* Does not print empty pages. */
//		if(pageIndex >= pages) {
//			return Printable.NO_SUCH_PAGE;
//		}

		/* Shifts the graphic to line up with the beginning of print-imageable region. */
		g2.translate(pf.getImageableX(), pf.getImageableY());
		
		/* Shifts the graphic to line up with the beginning of the next page to print. */
		//g2.translate(0f, -pageIndex*pageHeight);

		/* Scales the page so that the width fits. */
		g2.scale(scale, scale);

		/* Repaints the page. */
		componentToPrint.paint(g2);
 
		return Printable.PAGE_EXISTS;
	}
}