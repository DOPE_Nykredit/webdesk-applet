package com.nykredit.kundeservice.data;

import java.sql.SQLException;

public class DopeDBException extends DopeException{

	private static final long serialVersionUID = 1L;

	public DopeDBException(SQLException e, String exceptionMsg, String dopeErrorMsg){
		super(e, exceptionMsg, dopeErrorMsg);
	}
}
