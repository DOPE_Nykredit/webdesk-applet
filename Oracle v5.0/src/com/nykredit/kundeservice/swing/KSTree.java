package com.nykredit.kundeservice.swing;

import java.awt.Dimension;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import com.jidesoft.swing.CheckBoxTree;
import com.nykredit.kundeservice.data.CTIRConnection;

public class KSTree extends JScrollPane {
	private CheckBoxTree tree;
	private static final long serialVersionUID = 1L;
	private CTIRConnection oracle;
	private String  notInTeamGruppe = "'Basis', 'R�dgiver', 'Akademi', 'Ekspedition', 'Digitale'",
			Sql =		
"	SELECT AA.TEAM_NAVN, BB.INITIALER, AA.TEAM			"+
"	FROM KS_DRIFT.AGENTER_TEAMS AA						"+
"	INNER JOIN KS_DRIFT.V_TEAM_DATO BB					"+
"	ON AA.TEAM = BB.TEAM								"+
"	AND BB.DATO =TRUNC(SYSDATE)							"+
"	AND AA.TEAM_GRUPPE IN (" + notInTeamGruppe + ")	"+
"	ORDER BY AA.TEAM_NAVN, BB.INITIALER					";
	
	public static void main(String[] args) {
		CTIRConnection oracle = new CTIRConnection();
		try {
			oracle.Connect();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
	        UIManager.setLookAndFeel(
	            UIManager.getSystemLookAndFeelClassName());
	    } catch (Exception e) {}
		JFrame test = new JFrame();
		
		test.setContentPane(new KSTree(oracle));
		test.pack();
		test.setVisible(true);
	}
	
	public KSTree(CTIRConnection oracle) {
		this.setMinimumSize(new Dimension(195,350));
		this.setPreferredSize(new Dimension(195,350));
		this.oracle = oracle;
		tree = new CheckBoxTree();
		loadTree();
		this.setViewportView(tree);
		return;
	}
	
	public CheckBoxTree getTree(){
		return tree;
	}

	public void loadTree(){
		DefaultMutableTreeNode top = new DefaultMutableTreeNode("Servicecentret");
	    DefaultMutableTreeNode category = null;
	    DefaultMutableTreeNode book = null;
		try {
			ResultSet rs4 = oracle.Hent_tabel(Sql);
			String ged = "";
	        while(rs4.next()) {
	        	if (!ged.startsWith(rs4.getString("TEAM"))) {
	        		ged = rs4.getString("TEAM");
		            category = new DefaultMutableTreeNode(rs4.getString("TEAM_NAVN"));
		            top.add(category);
	        	}
	            book = new DefaultMutableTreeNode(rs4.getString("INITIALER"));
		        category.add(book);
			}
	        rs4.close();
		} catch (SQLException e1) {}
		DefaultTreeModel test = new DefaultTreeModel(top);
		tree.setModel(test);
	}

	
	public String getSelectionWhereClause(String VT, String AT){
		
		String team ="'SuperTroopers'";
		String initialer ="'XXXX'";
		String whereClause = "";
		boolean writeClause = false;
		
		try{
			for (TreePath i: tree.getCheckBoxTreeSelectionModel().getSelectionPaths()) {
				if (i.getPathCount() == 1){
					whereClause = " "+AT+"TEAM_GRUPPE IN ("+notInTeamGruppe+") ";
					break;	
				}
				else if (i.getPathCount() == 2 ) {
					team = team + ",'" + i.getLastPathComponent() + "'";
					writeClause = true;
				}
				else if (i.getPathCount() == 3) {
					initialer = initialer + ",'" + i.getLastPathComponent() + "'";	
					writeClause = true;
				}	
			}
			if (writeClause){
				whereClause = " " + AT + "TEAM_GRUPPE IN (" + notInTeamGruppe + ") AND " +
				"(" + AT + "TEAM_NAVN IN (" + team + ") OR " + VT + "INITIALER IN (" + initialer + ")) ";
			}
		} catch(Exception e){
			whereClause = " 1=2 ";
		}
		return whereClause;
	}
	
	
	
	
	
	
}