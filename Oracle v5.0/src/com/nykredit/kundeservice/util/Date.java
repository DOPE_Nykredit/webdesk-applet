package com.nykredit.kundeservice.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Date {
	private SimpleDateFormat sdfCalendar = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
	private SimpleDateFormat sdfFunctions = new SimpleDateFormat("yyyy-MM-dd");
    private Calendar cal = Calendar.getInstance();
    private com.nykredit.kundeservice.util.Formatter bs = new com.nykredit.kundeservice.util.Formatter();
    
	private java.util.Date parse(String ind) {
		try {return sdfFunctions.parse(ind);}
		catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Dato metoder XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */
////////////////////////////////////////////////// Skifter datoen 'ind' op eller ned, bestemt af 'on'
	public String datoadd(String ind, int on) { 
		Calendar c = Calendar.getInstance();
		c.setTime(parse(ind));
		c.add(Calendar.DATE, on);
		ind = sdfFunctions.format(c.getTime());  
		return ind; 
	}
	
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Private Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */
	private void trunc(int f, String nuls) {
		try {cal.setTime(sdfCalendar.parse(sdfCalendar.format(cal.getTime()).substring(0, f) + nuls));}
		catch (ParseException e) {}
	}
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Date methods with default XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */
/** Prints Date in the console in standard format, for testing purpose
 */	public void print() {
		System.out.println(sdfCalendar.format(cal.getTime()));
	}
/** Truncate Date to start of day
 */	public void trunc() {
	 	trunc(11, "00:00:00:000");
	}
/** Return Date in standard format
 */	public String ToString() {
		return sdfCalendar.format(cal.getTime());
	}
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Date methods XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */
/** Prints Date in the console in desired format, for testing purpose
 */	public void print(String form) {
		SimpleDateFormat f = new SimpleDateFormat(form);
		System.out.println(f.format(cal.getTime()));
	}
/** Truncate Date as desired (Y, M, W, D, h, m, s)
 */	public void trunc(char index) {
		switch (index) {
		case 'Y': trunc(5, "01-01 00:00:00:000");	break;	// Truncate year
		case 'M': trunc(8, "01 00:00:00:000");		break;	// Truncate month
		case 'W': edit('D', 1-getInfo('T'));				// Truncate week
		case 'D': trunc(11, "00:00:00:000");		break;	// Truncate day
		case 'h': trunc(14, "00:00:000");			break;	// Truncate hour
		case 'm': trunc(17, "00:000");				break;	// Truncate minute
		default:  trunc(20, "000");					break;	// Truncate second
		}
	}
/** Function to override Date (use get())
 */	public void override(String takein) {
		try {cal.setTime(sdfCalendar.parse(takein));}
		catch (ParseException e) {}
	}
/** Enter an optional information (Y, M, D, h, m, s) returns whether it succeeded
 */ public boolean set(char index, int value) {
	 	int Y = getInfo('Y');
	 	int M = getInfo('M');
	 	int D = getInfo('D');
	 	int h = getInfo('h');
	 	int m = getInfo('m');
	 	int s = getInfo('s');
		switch (index) {
		case 'Y': Y = value;		break;
		case 'M': M = value;		break;
		case 'D': D = value;		break;
		case 'h': h = value;		break;
		case 'm': m = value;		break;
		case 's': s = value;		break;
		default:  return false;
		}
	 	cal.set(Y, M, D, h, m, s);
	 	return true;
 	}
/** Edit an optional information (Y, M, D, h, m, s) returns whether it succeeded
 */ public boolean edit(char index, int value) {
		if (index == 'W') {return set('D', 7*value + getInfo('D'));}
		else {return set(index, value + getInfo(index));}
  	}
/** Return Date in desired format
 */	public String ToString(String form) {
		SimpleDateFormat f = new SimpleDateFormat(form);
		return f.format(cal.getTime());
	}
/** Return desired info as integer (Y, M, W, D, h, m, s)
 */ public int getInfo(char index) {
		switch (index) {
		case 'Y': return cal.get(Calendar.YEAR);
		case 'M': return cal.get(Calendar.MONTH);
		case 'W': return cal.get(Calendar.WEEK_OF_YEAR);
		case 'D': return cal.get(Calendar.DAY_OF_MONTH);
		case 'h': return cal.get(Calendar.HOUR_OF_DAY);
		case 'm': return cal.get(Calendar.MINUTE);
		case 's': return cal.get(Calendar.SECOND);
		default:  return 1 + (cal.get(Calendar.DAY_OF_WEEK) + 5) % 7;
		}
 	}
/** Return desired name as String (M, D)
 */ public String getName(char index) {
 		SimpleDateFormat f = null;
		switch (index) {
		case 'D': f = new SimpleDateFormat("EEEE");	break;
		case 'M': f = new SimpleDateFormat("MMMM");	break;
 		default:  return "";
		}
		try {return f.format(cal.getTime());}
 		catch(Exception e) {return "";}
 	}
/** Return true, if the date is greater than the reference
 */	public boolean isGreaterThan(String ref) {
		Calendar lac = Calendar.getInstance();
		try {lac.setTime(sdfCalendar.parse(ref));}
		catch (ParseException e1) {}
		return cal.getTimeInMillis() > lac.getTimeInMillis();
	}
////////////////////////////////////////////////// Retunerer navn p� dagen
public String dag(String dato) {
	String sample = dato;
	java.util.Date date = null;
    date = parse(sample);
	DateFormat f = new SimpleDateFormat("EEEE");
    try {return f.format(date);}
    catch(Exception e) {return "";}
  }
////////////////////////////////////////////////// Samler og retunerer dato udfra �r, m�ned of dag
public String dateserial(int dag,int mdr,int �r) {
Calendar cal;
String dato = �r+"-"+mdr+"-"+dag;
java.util.Date date = null;
date = parse(dato);
cal = Calendar.getInstance();
cal.setTime(date);
dato = sdfFunctions.format(date);//cal.get(Calendar.DATE));
return dato;
}
////////////////////////////////////////////////// Skifter datoen 'ind' op eller ned, s� den er i -b til f fra dd
public String datoLimit(String ind, int b, int f) {
	String nu = now();
	if (b > -1 && datoUp(datoadd(nu,-b), ind)) {
		ind = datoadd(nu,-b);
	} else if (f > -1 && datoUp(ind, datoadd(nu,f))) {
		ind = datoadd(nu,f);
	} return ind; 
}
////////////////////////////////////////////////// Retunerer sand, hvis f�rste dato er st�rre end sidste dato
public boolean datoUp(String stor, String lille) {
	Calendar s = Calendar.getInstance();
	Calendar l = Calendar.getInstance();
	s.setTime(parse(stor));
	l.setTime(parse(lille));
	return s.getTimeInMillis() > l.getTimeInMillis(); 
}
////////////////////////////////////////////////// Retunerer m�nednummer
public int mdr(String dato) {
    Calendar cal;
    java.util.Date date = null;
    int mdr;
    date = parse(dato);
    cal = Calendar.getInstance();
    cal.setTime(date);
    mdr = cal.get(Calendar.MONTH);
    return mdr;
}
////////////////////////////////////////////////// Retunerer m�nednavn
public String m�ned(String dato) {
	String sample = dato;
	java.util.Date date = null;
    date = parse(sample);
	DateFormat f = new SimpleDateFormat("MMMM");
    try {return f.format(date);}  catch(Exception e) {
      e.printStackTrace();
      return "";
    }
  }
////////////////////////////////////////////////// Retunerer dags dato
public String now() {
    Calendar cal = Calendar.getInstance();
    String dato = sdfFunctions.format(cal.getTime());
    return dato;
  }
////////////////////////////////////////////////// Retunerer dato i andet format
public String StringToDate(String text) {return StringToDate(text, "dd-MM-yyyy", "yyyy-MM-dd");}
public String StringToDate(String text, String fform) {return StringToDate(text, fform, "yyyy-MM-dd");}
public String StringToDate(String text, String fform, String tform) {
	SimpleDateFormat sdf = new SimpleDateFormat(fform);
	 
	java.util.Date today = parse(text);
	sdf = new SimpleDateFormat(tform);
	return sdf.format(today);
}
/** Converts HH:MM to time-at-day(second)
 */	public int toTimeS(String TTMM) {
		int val = Integer.valueOf(bs.cutFromFirst(TTMM,":"))*60;
		val += Integer.valueOf(bs.cutToFirst(TTMM,":").substring(1,3));
		return val;
	}
/** Converts time-at-day(day) to HH:MM
 */	public String toTTMM(double time, boolean ShowZero){
	 	if (time == 0 & ! ShowZero) return null;
		DecimalFormat myFormat = new DecimalFormat("00");
		String timer = myFormat.format((int)(time*24)).toString();
		String minutter = myFormat.format(Math.abs((int)(((time*24)-((int)(time*24)))*60)+0.5)).toString();
		String kl = timer + ":" + minutter;
		return kl;
	}
////////////////////////////////////////////////// Retunerer ugedag for given dato som tal
public int ugedag(String dato) {
    Calendar cal;
    java.util.Date date = null;
    int dag;
    date = parse(dato);
    cal = Calendar.getInstance();
    cal.setTime(date);
    dag = cal.get(Calendar.DAY_OF_WEEK);
    return dag;
}
/** returns day of month as integer*/
public int dayOfMonth(String dato) {
    Calendar cal;
    java.util.Date date = null;
    int dag;
    date = parse(dato);
    cal = Calendar.getInstance();
    cal.setTime(date);
    dag = cal.get(Calendar.DAY_OF_MONTH);
    return dag;
}
////////////////////////////////////////////////// Beregner ugenummer ud fra en dato
public int ugenr(String ind) {
    Calendar cal;
    java.util.Date date = null;
    int week;
    String sample = ind;
    date = parse(sample);
    cal = Calendar.getInstance();
    cal.setTime(date);
    week = cal.get(Calendar.WEEK_OF_YEAR) ;
    return week;
}
////////////////////////////////////////////////// Retunerer �r for given dato
public int �r(String dato) {
    Calendar cal;
    java.util.Date date = null;
    int aar;
    date = parse(dato);
    cal = Calendar.getInstance();
    cal.setTime(date);
    aar = cal.get(Calendar.YEAR);
    return aar;
}
}