package com.nykredit.kundeservice.util;

public class Formatter {
	
/** 
 * 
 * Checks if a given string is an integer
 * 
 * @param String
 * @return boolean
 */
	public boolean isInteger(String input) {
		try {Integer.parseInt(input); return true;}
		catch(Exception e) {return false;}   
	}
	
	/**
	 * Replaces a string with another string
	 * 
	 * @param text
	 * @param fra
	 * @param til
	 * @return String
	 */
    public String erstat(String text, String fra, String til) {
    	String result = "";
    	int part = fra.length();
    	for (int i=0; i < text.length(); i++) {
    		if (text.substring(i).startsWith(fra)) {
    			result += til;
    			i += part-1;
    		} else {
    			result += text.substring(i, i+1);
    		}
    	}
    	return result;
    }
    
   
    /**
     *  Cuts a String to start from a String
     *  
     * @param text
     * @param mark
     * @return
     */
    public String cutToFirst(String text, String mark) {
    	for (int i=1; i < text.length(); i++) {
    		if (text.substring(i).startsWith(mark)) {
    			text = text.substring(i);
    	    	break;
    		}
    	}
    	return text;
    }
    
    /**
     * Cuts a String from a String
     * 
     * @param text
     * @param mark
     * @return
     */
    public String cutFromFirst(String text, String mark) {
    	for (int i=1; i < text.length(); i++) {
    		if (text.substring(i).startsWith(mark)) {
    			try { text = text.substring(0, i); }
    			catch (Exception e) {}
    	    	break;
    		}
    	}
    	return text;
    }
    
    /**
     * Cuts pieces out of a text which is surrounded by two other strings
     * 
     * @param text
     * @param fra
     * @param til
     * @return
     */
    public String cutBetween(String text, String fra, String til) {
    	String result = "";
    	int from = 0;
    	boolean cutting = false;
    	for (int i=0; i < text.length(); i++) {
    		if (text.substring(i).startsWith(fra)) {
    			result += text.substring(from, i+1);
    			cutting = true;
    			from = i+1;
    		} else if (text.substring(i).startsWith(til) && cutting) {
    			cutting = false;
    			from = i;
    		}
    	}
		result += text.substring(from);
    	return result;
    }

/** Converts double value to %
 */	public String toProcent(double value, boolean ShowZero) {
		int val = (int)(value * 100 + 0.5); // Afrunder
		if (val < 1 & val > -1 & ! ShowZero) return null;
		return val + "%";
	}
/** Split large numbers with '.' as 1.234.567.890
 */	public String KiloDotFill(int value, boolean ShowZero) {
		if (value == 0 & ! ShowZero) return null;
		java.text.DecimalFormat myFormat = new java.text.DecimalFormat(",###");
		return myFormat.format(value);
	}
 
 	public String KiloDotFill(double value, boolean ShowZero) {
		if (value == 0 & ! ShowZero) return null;
		java.text.DecimalFormat myFormat = new java.text.DecimalFormat(",###");
		return myFormat.format(Math.round(value));
	}

}