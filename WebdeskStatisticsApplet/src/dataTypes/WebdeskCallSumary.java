package dataTypes;

import java.util.ArrayList;

import org.joda.time.DateTime;

import com.nykredit.kundeservice.data.DopeException;
import com.nykredit.kundeservice.data.KSDriftConnection;
import com.nykredit.kundeservice.data.sql.OracleFormats;
import com.nykredit.kundeservice.data.sql.SqlQuery;
import com.nykredit.kundeservice.util.OperationAbortedException;

public class WebdeskCallSumary {

	public enum SummaryPeriod{
		DAY("DDD"), WEEK("IW"), MONTH("MM");
		
		private String format;
		
		private SummaryPeriod(String format){
			this.format = format;
		}
	}
	
	private DateTime date;
			
	int callCount;
	int answeredCount;
	int answeredWithin30SecCount;
	int forwardToDirekteCount;
		
	public DateTime getDate(){
		return this.date;
	}
		
	public int getCallCount(){
		return this.callCount;
	}
	public int getAnsweredCount(){
		return this.answeredCount;
	}
	public int getAnsweredByServiceCenterCount(){
		return this.answeredCount - this.forwardToDirekteCount;
	}
	public int getAnsweredWithin30SecCount(){
		return this.answeredWithin30SecCount;
	}
	public int getForwardedToDirekteCount(){
		return this.forwardToDirekteCount;
	}

	public WebdeskCallSumary(DateTime date,
							 int callCount,
							 int answeredCount,
							 int answeredWithin30SecCount,
							 int forwardToDirekteCount){
		this.date = date;
		
		this.callCount = callCount;
		this.answeredCount = answeredCount;
		this.answeredWithin30SecCount = answeredWithin30SecCount;
		this.forwardToDirekteCount = forwardToDirekteCount;
	}

	public static ArrayList<WebdeskCallSumary> getWebdeskCalls(KSDriftConnection conn, 
															   DateTime dateFrom, 
															   DateTime dateTo,
															   SummaryPeriod period) throws DopeException {
		if(conn == null) throw new IllegalArgumentException("Conn cannot be null.");
		if(dateFrom == null) throw new IllegalArgumentException("DateFrom cannot be null.");
		if(dateTo == null) throw new IllegalArgumentException("DateTo cannot be null.");
		if(period == null) throw new IllegalArgumentException("SummaryPeriod cannot be null.");
		
		ArrayList<WebdeskCallSumary> webdeskCalls = new ArrayList<WebdeskCallSumary>();
				
		String sql = "SELECT " + 
					     "TO_CHAR(DATETIME, '" + period.format + "'), " +
					     "TRUNC(MIN(DATETIME)) AS PERIOD_MIN_DATE, " +
					     "COUNT(*) AS CALLS, " +
					     "COUNT(CASE WHEN web.INITIALS is not null THEN 1 END) AS ANSWERED, " +
					     "COUNT(CASE WHEN vt.INITIALER is not null AND web.RESPONSE_TIME < 31 THEN 1 END) AS ANSWERED_WITHIN_30_SEC, " +
					     "COUNT(CASE WHEN web.INITIALS is not null AND vt.INITIALER is null THEN 1 END) AS FORWARDED_TO_DIREKTE " +
					 "FROM " + 
					 	 "KS_DRIFT.V_WEBDESK_SERVICECENTER web " + 
					 "LEFT JOIN " + 
					 	 "KS_DRIFT.V_TEAM_DATO vt ON web.INITIALS = vt.INITIALER AND trunc(web.DATETIME) = vt.DATO " +
					 "WHERE " + 
					 	 "DATETIME BETWEEN " + OracleFormats.convertDateTime(dateFrom) + " AND " + OracleFormats.convertDateTime(dateTo) + " " +
					 "GROUP BY " + 
					 	 "TO_CHAR(DATETIME, 'YYYY'), " + 
					 	 "TO_CHAR(DATETIME, '" + period.format + "')";
		
		SqlQuery query = new SqlQuery(conn, sql);
		
		try {
			query.execute();
			
			while(query.next()){				
				webdeskCalls.add(new WebdeskCallSumary(query.getDateTime("PERIOD_MIN_DATE"), 
													   query.getInteger("CALLS"),
													   query.getInteger("ANSWERED"),
													   query.getInteger("ANSWERED_WITHIN_30_SEC"),
													   query.getInteger("FORWARDED_TO_DIREKTE")));				
			}
		} catch (DopeException e) {
			throw new OperationAbortedException(e, "Error during load of webdesk calls.", "Der skete en fejl ved indlęsning af webdesk kald.");
		}
		
		return webdeskCalls;
	}
}