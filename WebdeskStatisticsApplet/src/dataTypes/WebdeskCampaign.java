package dataTypes;

import java.util.ArrayList;

import com.nykredit.kundeservice.data.DopeException;
import com.nykredit.kundeservice.data.KSDriftConnection;
import com.nykredit.kundeservice.data.sql.SqlQuery;
import com.nykredit.kundeservice.util.OperationAbortedException;
import com.nykredit.kundeservice.util.StringUtils;

public class WebdeskCampaign {

	private int id;
	private String name;
	
	public int getId(){
		return this.id;
	}
	public String getName(){
		return this.name;
	}
	public String getFormattedName(){
		String formattedName = this.name;
		
		formattedName.replace('_', ' ');
		
		return StringUtils.setFirstWordUpperCase(formattedName);
	}
	
	public WebdeskCampaign(int id, String name){
		this.id = id;
		this.name = name;
	}
	@Override
	public String toString() {
		return this.getFormattedName();
	}
	
	public static WebdeskCampaign FindCampaignById(ArrayList<WebdeskCampaign> campaigns, int campaignId){
		if(campaigns == null)
			throw new IllegalArgumentException("Campaigns cannot be null.");
		
		for(WebdeskCampaign c : campaigns)
			if(c.getId() == campaignId)
				return c;
		
		return null;
	}
	public static ArrayList<WebdeskCampaign> getAllCampaigns(KSDriftConnection conn) throws OperationAbortedException{
		if(conn == null)
			throw new IllegalArgumentException("Conn cannot be null.");
		
		ArrayList<WebdeskCampaign> campaigns = new ArrayList<WebdeskCampaign>();
		
		String sql = "SELECT " +
					     "* " +
					 "FROM " +
					     "KS_DRIFT.WEBDESK_CAMPAIGN";
		
		SqlQuery query = new SqlQuery(conn, sql);
		
		try {
			query.execute();
			
			while(query.next())
				campaigns.add(new WebdeskCampaign(query.getInteger("ID"), query.getString("CAMPAIGN")));
		} catch (DopeException e) {
			throw new OperationAbortedException(e, "Error during load of webdesk campaigns.", "Der skete en fejl ved indlęsning af webdesk campaigns.");
		}
	
		return campaigns;
	}
}