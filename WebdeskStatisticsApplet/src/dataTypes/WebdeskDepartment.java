package dataTypes;

import java.util.ArrayList;

import com.nykredit.kundeservice.data.DopeException;
import com.nykredit.kundeservice.data.KSDriftConnection;
import com.nykredit.kundeservice.data.sql.SqlQuery;
import com.nykredit.kundeservice.util.OperationAbortedException;

public class WebdeskDepartment {

	private int id;
	private String name;

	public int getId(){
		return this.id;
	}
	public String getName(){
		return this.name;
	}

	public WebdeskDepartment(int id, String name){
		this.id = id;
		this.name = name;
	}
	@Override
	public String toString() {
		return this.name;
	}

	public static WebdeskDepartment FindDepartmentById(ArrayList<WebdeskDepartment> departments, int departmentId){
		if(departments == null)
			throw new IllegalArgumentException("Departments cannot be null.");
		
		for(WebdeskDepartment d : departments)
			if(d.getId() == departmentId)
				return d;
		
		return null;
	}
	public static ArrayList<WebdeskDepartment> getAllDepartments(KSDriftConnection conn) throws OperationAbortedException{
		if(conn == null)
			throw new IllegalArgumentException("Conn cannot be null.");

		ArrayList<WebdeskDepartment> departments = new ArrayList<WebdeskDepartment>(); 

		String sql = "SELECT " +
					     "* " +
					 "FROM " +
					     "KS_DRIFT.WEBDESK_DEPARTMENT";

		SqlQuery query = new SqlQuery(conn, sql);

		try {
			query.execute();
			
			while(query.next())
				departments.add(new WebdeskDepartment(query.getInteger("ID"), query.getString("DEPARTMENT")));
		} catch (DopeException e) {
			throw new OperationAbortedException(e, "Error during load of webdesk departments.", "Der skete en fejl ved indlęsning af webdesk departments.");
		}

		return departments;
	}
}