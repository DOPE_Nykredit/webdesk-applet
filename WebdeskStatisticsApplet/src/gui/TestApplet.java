package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.SystemColor;
import java.util.ArrayList;

import javax.swing.JApplet;
import javax.swing.JPanel;
import javax.swing.UIManager;

import org.joda.time.DateTime;

import com.nykredit.kundeservice.data.DopeDBException;
import com.nykredit.kundeservice.data.DopeException;
import com.nykredit.kundeservice.data.KSDriftConnection;
import com.nykredit.kundeservice.swing.PeriodSelector;

import dataTypes.WebdeskCallSumary;
import dataTypes.WebdeskCallSumary.SummaryPeriod;

public class TestApplet extends JApplet {

	private static final long serialVersionUID = 1L;
	private PeriodSelector periodSelector;

	public TestApplet() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.getContentPane().setSize(new Dimension(780, 515));
		this.getContentPane().setPreferredSize(new Dimension(780, 515));
		this.getContentPane().setMinimumSize(new Dimension(780, 515));
		this.getContentPane().setMaximumSize(new Dimension(780, 515));
		
		this.setSize(new Dimension(780, 515));
		this.setMinimumSize(new Dimension(780, 515));
		this.setMaximumSize(new Dimension(780, 515));
		this.setPreferredSize(new Dimension(780, 515));
		
		JPanel dateSelectorPanel = new JPanel();
		dateSelectorPanel.setBackground(SystemColor.window);
		FlowLayout fl_dateSelectorPanel = (FlowLayout) dateSelectorPanel.getLayout();
		fl_dateSelectorPanel.setAlignment(FlowLayout.LEFT);
		this.getContentPane().add(dateSelectorPanel, BorderLayout.NORTH);
		
		this.periodSelector = new PeriodSelector(null, null, 13);
		this.add(this.periodSelector);
		periodSelector.setBackground(SystemColor.window);
		
		try {
			ArrayList<WebdeskCallSumary> webdeskCalls = WebdeskCallSumary.getWebdeskCalls(new KSDriftConnection(), 
			   			  new DateTime(2013, 1, 10, 0, 0), 
			   			  new DateTime(2013, 3, 1, 0,0),
			   			  SummaryPeriod.DAY);
		} catch (DopeDBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DopeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
