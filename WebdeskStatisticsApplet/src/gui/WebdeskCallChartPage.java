package gui;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.TextAnchor;

import dataTypes.WebdeskCallSumary;

public class WebdeskCallChartPage extends JPanel {

	private static final long serialVersionUID = 1L;

	public enum WebdeskCallChartPeriod{
		DAY("Dag", "d. MMM /yy", Calendar.DATE),
		WEEK("Uge", "w /yy", Calendar.WEEK_OF_YEAR),
		MONTH("M�ned", "MMM /yy", Calendar.MONTH);

		private String name;
		private String formatString;
		private int CalendarField;

		private WebdeskCallChartPeriod(String name, String formatString, int calendarField){
			this.name = name;
			this.formatString = formatString;
			this.CalendarField = calendarField;
		}

		private String getFormatString(){
			return this.formatString;
		}
		
		private int getCalendarField(){
			return this.CalendarField;
		}
		
		@Override
		public String toString() {
			return this.name;
		}
	}
	
	private WebdeskCallChartPeriod chartPeriod = WebdeskCallChartPeriod.DAY;
	
	private Date lowerBound;
	private Date upperBound;

	public WebdeskCallChartPeriod getChartPeriod(){
		return this.chartPeriod;
	}
	public void setChartPeriod(WebdeskCallChartPeriod chartPeriod){
		this.chartPeriod = chartPeriod;
	}

	public WebdeskCallChartPage(){
	}

	public void DisplayWebdeskChart(ArrayList<WebdeskCallSumary> webdeskCalls, Date lowerBound, Date upperBound){		
		DefaultCategoryDataset callAmountDataSet = new DefaultCategoryDataset();
		DefaultCategoryDataset handlingDegreeDataSet = new DefaultCategoryDataset();
		
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
		
		Collections.sort(webdeskCalls, new Comparator<WebdeskCallSumary>() {
			@Override
			public int compare(WebdeskCallSumary o1, WebdeskCallSumary o2) {
				return o1.getDate().compareTo(o2.getDate());
			}
		});
		
		for(WebdeskCallsByPeriod w : this.categorizeWebdeskCallsSelectedByPeriod(webdeskCalls)){
			callAmountDataSet.addValue(w.getAnswered(), "Besvarede", w.getPeriodName());
			callAmountDataSet.addValue(w.getMissed(), "Ikke besvarede", w.getPeriodName());
			
			handlingDegreeDataSet.addValue(w.getHandlingDegree(), "Svarprocent", w.getPeriodName());
		}
		
		JFreeChart callChart = ChartFactory.createStackedBarChart("Webdesk kald", 
														   		  this.chartPeriod.toString(), 
														   		  "Antal", 
														   		  callAmountDataSet, 
														   		  PlotOrientation.VERTICAL, 
														   		  true, 
														   		  false, 
														   		  false);
		
		callChart.getPlot().setBackgroundPaint(Color.WHITE);
		
		CategoryPlot plot = callChart.getCategoryPlot();
		plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);
		
		plot.setDataset(1, handlingDegreeDataSet);
				
		plot.setRangeGridlinePaint(Color.DARK_GRAY);
		plot.getRangeAxis().setLabel("Antal kald");
		
		plot.getDomainAxis().setCategoryLabelPositions(CategoryLabelPositions.DOWN_45);
		
		ValueAxis secondAxis = new NumberAxis("Svarprocent");
		secondAxis.setAutoRange(false);
		secondAxis.setLowerBound(0);
		secondAxis.setUpperBound(105);
		secondAxis.setTickLabelFont(plot.getRangeAxis(0).getTickLabelFont());
		
		plot.setRangeAxis(1, secondAxis);
		plot.mapDatasetToRangeAxis(1, 1);
		plot.setNoDataMessage("Hej");
		CategoryItemRenderer barRenderer = plot.getRenderer(0);
		barRenderer.setSeriesPaint(0, Color.GREEN);
		barRenderer.setSeriesPaint(1, Color.RED);
		
		barRenderer.setBaseItemLabelsVisible(true);   
		barRenderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());   
		for (int i=0; i<3; i++)   
		    barRenderer.setSeriesPositiveItemLabelPosition(i, new ItemLabelPosition(ItemLabelAnchor.CENTER, TextAnchor.CENTER)); //, TextAnchor.CENTER, 1.5707964  
				
		LineAndShapeRenderer lineRenderer = new LineAndShapeRenderer();
		lineRenderer.setSeriesPaint(0, Color.BLUE);
		plot.setRenderer(1, lineRenderer);
		
		this.removeAll();
		this.add(new ChartPanel(callChart));
		
		this.validate();
	}

	private ArrayList<WebdeskCallsByPeriod> categorizeWebdeskCallsSelectedByPeriod(ArrayList<WebdeskCallSumary> webdeskCalls){
		ArrayList<WebdeskCallsByPeriod> allCallsByPeriod = new ArrayList<WebdeskCallsByPeriod>();
		
		Calendar dateIterator = Calendar.getInstance();
		Calendar upperBound = Calendar.getInstance();
		upperBound.setTime(this.upperBound);
		
		dateIterator.setTime(this.lowerBound);
		
		while((dateIterator.after(upperBound) == false)){
			String periodName = new SimpleDateFormat(this.chartPeriod.getFormatString()).format(dateIterator.getTime());
			
			allCallsByPeriod.add(new WebdeskCallsByPeriod(periodName, 0, 0));
			
			dateIterator.add(this.chartPeriod.getCalendarField(), 1);
		}
		
		SimpleDateFormat periodFormat = new SimpleDateFormat(this.chartPeriod.getFormatString());
		
//		for(WebdeskCallSumary c : webdeskCalls){
//			WebdeskCallsByPeriod callsBySinglePeriod = WebdeskCallChartPage.FindCallsByPeriodByPeriodName(allCallsByPeriod,
//																										  periodFormat.format(c.getDateTime()));
//			
//			if(callsBySinglePeriod != null){
//				if(c.getInitials() != null)
//					callsBySinglePeriod.addAnswered(1);
//				else
//					callsBySinglePeriod.addMissed(1);	
//			}
//		}

		return allCallsByPeriod;
	}
	
	private static WebdeskCallsByPeriod FindCallsByPeriodByPeriodName(ArrayList<WebdeskCallsByPeriod> callsByPeriod, String periodName){
		for(WebdeskCallsByPeriod w : callsByPeriod)
			if(w.periodName.equals(periodName))
				return w;
		
		return null;
	}

	private class WebdeskCallsByPeriod{

		private String periodName;

		private int answered = 0;
		private int missed = 0;

		public String getPeriodName(){
			return this.periodName;
		}
		
		public int getAnswered(){
			return this.answered;
		}
		public void addAnswered(int answered){
			this.answered += answered;
		}
		
		public int getMissed(){
			return this.missed;
		}
		public void addMissed(int missed){
			this.missed += missed;
		}

		public int getTotalCalls(){
			return this.answered + this.missed;
		}
		public double getHandlingDegree(){
			return (double)answered / (double)this.getTotalCalls() * 100;
		}

		private WebdeskCallsByPeriod(String periodName, int answered, int missed){
			this.periodName = periodName;

			this.answered = answered;
			this.missed = missed;
		}
		@Override
		public boolean equals(Object obj) {
			if(obj == null) return false;
			if(obj instanceof WebdeskCallsByPeriod == false) return false;
			return (this.periodName.equals(((WebdeskCallsByPeriod)obj).periodName));
		}
	}
}