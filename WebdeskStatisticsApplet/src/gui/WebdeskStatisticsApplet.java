package gui;

import javax.swing.JApplet;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JPanel;

import com.nykredit.kundeservice.data.DopeException;
import com.nykredit.kundeservice.data.KSDriftConnection;
import com.nykredit.kundeservice.swing.PeriodSelector;
import com.nykredit.kundeservice.swing.PeriodSelector.SelectionMode;
import com.nykredit.kundeservice.util.StringUtils;

import dataTypes.WebdeskCallSumary;
import dataTypes.WebdeskCallSumary.SummaryPeriod;
import gui.WebdeskCallChartPage.WebdeskCallChartPeriod;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.EnumSet;
import java.awt.SystemColor;

public class WebdeskStatisticsApplet extends JApplet {
	
	private final JPanel GraphPanel = new JPanel();
	
	private PeriodSelector periodSelector;
	
	private final WebdeskCallChartPage webdeskChartPanel = new WebdeskCallChartPage();
	
	public WebdeskStatisticsApplet() {
		getContentPane().setSize(new Dimension(780, 515));
		getContentPane().setPreferredSize(new Dimension(780, 515));
		getContentPane().setMinimumSize(new Dimension(780, 515));
		getContentPane().setMaximumSize(new Dimension(780, 515));
		
		this.setSize(new Dimension(780, 515));
		this.setMinimumSize(new Dimension(780, 515));
		this.setMaximumSize(new Dimension(780, 515));
		this.setPreferredSize(new Dimension(780, 515));
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		JPanel dateSelectorPanel = new JPanel();
		dateSelectorPanel.setBackground(SystemColor.window);
		FlowLayout fl_dateSelectorPanel = (FlowLayout) dateSelectorPanel.getLayout();
		fl_dateSelectorPanel.setAlignment(FlowLayout.LEFT);
		getContentPane().add(dateSelectorPanel, BorderLayout.NORTH);
		
		this.periodSelector = new PeriodSelector(null, null, 13);
		periodSelector.setBackground(SystemColor.window);
		
		EnumSet<SelectionMode> selectionModes;
		selectionModes = EnumSet.of(SelectionMode.DAY, SelectionMode.WEEK, SelectionMode.MONTH);
		periodSelector.setSelectionModes(selectionModes);
		dateSelectorPanel.add(periodSelector);
		
		JButton btnGetData = new JButton("Hent data");
		btnGetData.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				WebdeskStatisticsApplet.this.LoadWebdeskData();
			}
		});
		dateSelectorPanel.add(btnGetData);
		
		this.GraphPanel.setLayout(new BorderLayout(0, 0));
		this.getContentPane().add(this.GraphPanel, BorderLayout.CENTER);
		
		this.webdeskChartPanel.setBackground(SystemColor.window);
		this.GraphPanel.add(this.webdeskChartPanel, BorderLayout.CENTER);
		
		SwingUtilities.updateComponentTreeUI(this);
	}

	private static final long serialVersionUID = 1L;
	
	private void LoadWebdeskData(){
		try {
			SummaryPeriod period = SummaryPeriod.DAY;
			
			switch(this.periodSelector.getCurrentSelectedMode()){
				case DAY:
					webdeskChartPanel.setChartPeriod(WebdeskCallChartPeriod.DAY);
					period = SummaryPeriod.DAY;
					break;
				case WEEK:
					webdeskChartPanel.setChartPeriod(WebdeskCallChartPeriod.WEEK);
					period = SummaryPeriod.WEEK;
					break;
				case MONTH:
					webdeskChartPanel.setChartPeriod(WebdeskCallChartPeriod.MONTH);
					period = SummaryPeriod.MONTH;
					break;
			}
			
			ArrayList<WebdeskCallSumary> webdeskCalls = WebdeskCallSumary.getWebdeskCalls(new KSDriftConnection(), 
					       													   			  this.periodSelector.getSelectedPeriodLowerBound(), 
					       													   			  this.periodSelector.getSelectedPeriodUpperBound(),
					       													   			  period);
			
//			this.webdeskChartPanel.DisplayWebdeskChart(webdeskCalls, 
//													   this.periodSelector.getSelectedPeriodLowerBound(),
//													   this.periodSelector.getSelectedPeriodUpperBound());
		} catch (DopeException e) {
			System.out.println("Fejl");
		}
	}
}
